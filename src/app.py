import argparse
import logging
import logging.config
import sys
from palindrome.engine import PalindromicApp


LOG = logging.getLogger(__name__)

DEFAULT_MAX_NUMBER = 1000
DEFAULT_MAX_BASE = sys.maxsize
DEFAULT_LOG_LEVEL = 'INFO'

parser = argparse.ArgumentParser(
    description='Telnyx Palindrome Python Challenge')
parser.add_argument('--max_base',
                    action="store",
                    type=int, help='Up to maximum base to test',
                    default=DEFAULT_MAX_BASE)
parser.add_argument('--max_number',
                    action="store",
                    type=int,
                    help='Up to maximum decimal number to test',
                    default=DEFAULT_MAX_NUMBER)
parser.add_argument('-v',
                    action='store_true',
                    default=False,
                    help='Enable verbose logging output')

args = parser.parse_args()

if args.v:
    DEFAULT_LOG_LEVEL = 'DEBUG'

# Sensible logging defaults
LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': '%(asctime)s":[%(levelname)s]:'
                      '[%(process)d-%(thread)d]:[%(name)s]:'
                      '[%(funcName)s]: %(message)s'
        },
        'simple': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        },
    },
    'handlers': {
        'file_debug': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': DEFAULT_LOG_LEVEL,
            'filename': 'palindrome.log',
            'maxBytes': 10485760,
            'backupCount': 10,
            'encoding': 'utf8',
            'formatter': 'verbose',
        },
        'console': {
            'level': DEFAULT_LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
            'stream': 'ext://sys.stdout'
        }
    },
    'loggers': {
        '__main__':
            {
                'propagate': 'yes',
                'level': DEFAULT_LOG_LEVEL
            },
        'palindrome':
            {
                'propagate': 'yes',
                'level': DEFAULT_LOG_LEVEL
            },
        'tests':
            {
                'propagate': 'yes',
                'level': DEFAULT_LOG_LEVEL
            }
    },

    'root': {
        'propagate': 'yes',
        'handlers': ['console', 'file_debug']
    }
}

# Setup loggers
logging.config.dictConfig(LOGGING)
logging.getLogger().setLevel(DEFAULT_LOG_LEVEL)

# Start app
app = PalindromicApp(max_base=args.max_base, max_number=args.max_number)
app.run()
# Show results
print(app.retrieve_output())
