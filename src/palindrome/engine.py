import logging
import sys

from io import StringIO

LOG = logging.getLogger(__name__)


class UnableToConvertException(Exception):
    """
    Thrown when the number is negative or the base
    is larger than the provided or default symbol table
    """


class NotPalindromeException(Exception):
    """
    Raised when the number is not a palindrome
    """
    pass


class InvalidArbitraryNumberException(Exception):
    """
    Thrown by PalindromeNumber when the input is not a valid
    ArbitraryBaseNumberFromBase10 instance
    """


class PalindromeNumber:
    """
    Represents a palindrome number otherwise
    Throws an exception if the number is not a palindrome
    """

    def __init__(self, number):
        # Number has to be an instance of ArbitraryBaseNumberFromBase10
        # This will allow more flexibility to represent an arbitrary
        # number of symbols

        if isinstance(number, ArbitraryBaseNumberFromBase10):
            self.number = number
            if not self._is_palindrome():
                msg = 'Number {0} is NOT a palindrome.' \
                    .format(str(self.number))
                LOG.debug(msg)
                raise NotPalindromeException(msg)
            else:
                LOG.debug('Number {0} is a palindrome.'
                          .format(str(self.number)))
        else:
            raise InvalidArbitraryNumberException(
                'Number has to be an instance of {0}'.format(
                    ArbitraryBaseNumberFromBase10.__class__.__name__))

    def _is_palindrome(self):
        """
        Checks if the number is actually a palindrome or not
        :return: Boolean
        """
        LOG.debug('Number: {0}'.format(str(self.number)))
        LOG.debug('Reversed: {0}'.format(reversed(self.number)))
        if str(self.number) == str(reversed(self.number)):
            return True
        else:
            return False

    def __str__(self):
        return str(self.number)

    def __repr__(self):
        return self.__str__()


class ArbitraryBaseNumberFromBase10:
    """
    Converts a base10 number to one with arbitrary base
    NOTE: Due to lack of alphabet letters
          we can convert up to base 36.
          Known limitation for this implementation.

          * Increased the symbols to be used to represent a digit to 100
            (all printable characters)

          * Thinking about another way of representing digits, perhaps
            with a digit separator and permutations/combinations (or
            both) of printable characters to scale up indefinitely.
          * Implemented a list of integers as representation of the
            symbol table, and this allowed to scale much better than
            what I was thinking before.
            We can go up to sys.maxsize now.
    """

    def __init__(self, number, base):
        # Removed symbol table option since its more effecive to
        # use a list of integers as symbols and deal with the list
        # since we are not looking to print the representation, but
        # the base number
        self.base10 = int(number)
        self.new_base = int(base)

    def base(self):
        """
        Getter for base
        :return:
        """
        return int(self.new_base)

    def number(self):
        """
        Convert positive decimal integer n to equivalent in
        another base (2 - size of the symbol table)
        """

        try:
            base10_number = int(self.base10)
            new_base = int(self.new_base)
            LOG.debug('Base: {0}'.format(str(new_base)))
        except:
            raise Exception('Number or base too big to handle as integer!')

        if base10_number < 0 or new_base < 2:
            # Ignore non-positive numbers
            # and/or base smaller than 2
            raise UnableToConvertException('Unable to convert - {0}'
                                           .format(str(self.base10)))

        if base10_number == 0:
            # Easiest case
            return [0]

        output = []

        while base10_number:
            # Adapted the algorithm to use a list of integers
            # as representation. This allows for very large
            # arbitrary number of symbol representation

            remainder = int(base10_number % new_base)
            output.append(int(remainder))

            # Made sure to use floor division
            # as we don't want float numbers here
            base10_number //= new_base

            LOG.debug('Number: {0}'.format(str(base10_number)))
            LOG.debug('Remainder: {0}'
                      .format(str(int(base10_number % new_base))))

        LOG.debug('Final number representation: {0}'.format(str(output[::-1])))
        return output[::-1]

    def base10(self):
        """
        Getter for the original base10 number
        :return:
        """
        return self.base10

    def __repr__(self):
        return str(self.number())

    def __str__(self):
        return self.__repr__()

    def __eq__(self, other):
        """
        Override the default Equals behavior and use the
        underlying output list to compare the object
        """
        if isinstance(other, self.__class__):
            return self.number() == other.number()
        return False

    def __reversed__(self):
        """
        Override the default reversed behaviour and
        reverse the underlying output list
        :return:
        """
        return list(reversed(self.number()))


class PalindromicApp:
    """
    Encapsulate application logic
    """

    output = StringIO()

    def __init__(self, max_number=1000, max_base=sys.maxsize):
        # Sensible defaults
        self.max_number = max_number
        self.max_base = max_base

    def run(self):
        # Start Problem logic #
        # Test positive decimal numbers up to max_number (1000 default)
        self.output.write('\"decimal\", \"'
                          'smallest base in which the '
                          'number is a palindrome\"\n')
        for i in range(0, self.max_number):

            # from base 2 to max_base
            for b in range(2, self.max_base):

                try:
                    num = ArbitraryBaseNumberFromBase10(i, b)
                    LOG.debug('Decimal: {0} - '
                              'Base[{1}]: {2}'.format(str(i), str(b), num))

                except UnableToConvertException as e:
                    self.output.write('{0}, '
                                      'Exceeded symbol table size '
                                      'for base[{1}]!'
                                      '\n'.format(str(i), str(b)))
                    LOG.debug(
                        '{0}, {1} - base range exceeded, unable to '
                        'find minimum base where this number is '
                        'palindromic!'.format(
                            str(i), str(b)))
                    break

                try:
                    PalindromeNumber(num)
                    self.output.write('{0}, {1}\n'.format(str(i), str(b)))
                    # Add to final list & continue with the next number
                    break
                except NotPalindromeException as e:
                    if b == self.max_base - 1:
                        self.output.write('{0}, Exceeded set '
                                          'maximum size for base[{1}]!'
                                          '\n'.format(str(i), str(b)))

                except InvalidArbitraryNumberException as e:
                    self.output.write('{0}, Invalid '
                                      'input number and/or '
                                      'base!\n'.format(str(i)))

    def retrieve_output(self):
        return self.output.getvalue()
