import json
import os

from palindrome.engine import ArbitraryBaseNumberFromBase10

# Read JSON static data file
PROJECT_PATH = os.path.abspath(
    os.path.dirname(__file__))
with open(PROJECT_PATH +
          '/data.json') as data_file:
    DATA = json.load(data_file)


def assert_result(n, base):
    # Keys are required to be string due to
    # json.load bringing them back as strings
    assert ArbitraryBaseNumberFromBase10(
        n, base).number() == DATA[str(base)][str(n)]


class TestArbitraryBaseNumber(object):
    def test_arbitrary_base2(self):
        for i in range(0, 1000):
            yield assert_result, i, 2

    def test_arbitrary_base8(self):
        for i in range(0, 1000):
            yield assert_result, i, 8

    def test_arbitrary_base16(self):
        for i in range(0, 1000):
            yield assert_result, i, 16

    def test_arbitrary_base36(self):
        for i in range(0, 1000):
            yield assert_result, i, 36

    def test_arbitrary_base100(self):
        for i in range(0, 1000):
            yield assert_result, i, 100

    def test_arbitrary_base500(self):
        for i in range(0, 1000):
            yield assert_result, i, 500

    def test_arbitrary_base750(self):
        for i in range(0, 1000):
            yield assert_result, i, 750

    def test_arbitrary_base999(self):
        for i in range(0, 1000):
            yield assert_result, i, 999
