import os
from unittest import TestCase

import logging
import logging.config

from palindrome.engine import PalindromicApp

# Read output results data file
PROJECT_PATH = os.path.abspath(
    os.path.dirname(__file__))
with open(PROJECT_PATH +
          '/results_data') as file:
    static_output = file.read()


class TestPalindromeNumber(TestCase):

    def test_palindromic_app_1000_max_number_size(self):
        # Full 1000 numbers test
        logging.getLogger().setLevel('INFO')
        app = PalindromicApp()
        app.run()
        self.assertEqual(app.retrieve_output(), static_output)
