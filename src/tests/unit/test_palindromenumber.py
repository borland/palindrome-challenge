from unittest import TestCase
from palindrome.engine import PalindromeNumber, \
    NotPalindromeException, ArbitraryBaseNumberFromBase10, \
    InvalidArbitraryNumberException


class TestPalindromeNumber(TestCase):
    def test_palindrome_exception_1(self):
        with self.assertRaises(NotPalindromeException):
            PalindromeNumber(ArbitraryBaseNumberFromBase10(100, 2))

    def test_palindrome_exception_2(self):
        with self.assertRaises(InvalidArbitraryNumberException):
            PalindromeNumber(100)

    def test_valid_palindrome_0(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(0, 10))
        self.assertEqual(str(pal), '[0]')

    def test_valid_palindrome_1(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(1, 2))
        self.assertEqual(str(pal), '[1]')

    def test_valid_palindrome_2(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(2, 3))
        self.assertEqual(str(pal), '[2]')

    def test_valid_palindrome_3(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(3, 2))
        self.assertEqual(str(pal), '[1, 1]')

    def test_valid_palindrome_4(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(4, 3))
        self.assertEqual(str(pal), '[1, 1]')

    def test_valid_palindrome_5(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(5, 2))
        self.assertEqual(str(pal), '[1, 0, 1]')

    def test_valid_palindrome_6(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(6, 5))
        self.assertEqual(str(pal), '[1, 1]')

    def test_valid_palindrome_7(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(7, 2))
        self.assertEqual(str(pal), '[1, 1, 1]')

    def test_valid_palindrome_8(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(8, 3))
        self.assertEqual(str(pal), '[2, 2]')

    def test_valid_palindrome_9(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(9, 2))
        self.assertEqual(str(pal), '[1, 0, 0, 1]')

    def test_valid_palindrome_10(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(10, 3))
        self.assertEqual(str(pal), '[1, 0, 1]')

    def test_valid_palindrome_11(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(11, 10))
        self.assertEqual(str(pal), '[1, 1]')

    def test_valid_palindrome_12(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(12, 5))
        self.assertEqual(str(pal), '[2, 2]')

    def test_valid_palindrome_13(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(13, 3))
        self.assertEqual(str(pal), '[1, 1, 1]')

    def test_valid_palindrome_14(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(14, 6))
        self.assertEqual(str(pal), '[2, 2]')

    def test_valid_palindrome_15(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(15, 2))
        self.assertEqual(str(pal), '[1, 1, 1, 1]')

    def test_valid_palindrome_16(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(16, 3))
        self.assertEqual(str(pal), '[1, 2, 1]')

    def test_valid_palindrome_17(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(17, 2))
        self.assertEqual(str(pal), '[1, 0, 0, 0, 1]')

    def test_valid_palindrome_18(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(18, 5))
        self.assertEqual(str(pal), '[3, 3]')

    def test_valid_palindrome_19(self):
        pal = PalindromeNumber(ArbitraryBaseNumberFromBase10(19, 18))
        self.assertEqual(str(pal), '[1, 1]')
