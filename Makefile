CWD=$(shell pwd)
PYVER ?= $(shell for p in python3; do \
	out=$(which $$p 2>&1) && echo $$p && exit; done; \
	exit 1)
noseopts ?= -v

ifeq ($(PYVER),python3)
  unittests = unittest3
else
  pyflakes = pyflakes pyflakes3
  unittests = unittest unittest3
endif

all: pep8 unittest3 test run

pep8:
	pep8 src/

unittest3: clean_pyc
	nosetests3 $(noseopts) src/tests/unit

test: $(unittests)

clean_pyc:
	@find . -type f -name "*.pyc" -delete

run:
	${PYVER} src/app.py

